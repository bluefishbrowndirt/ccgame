from django.db import models


from countries.models import Nation


class Region(models.Model):

    name = models.CharField(max_length=32, blank=False)

    def __str__(self):
        return self.name


class Territory(models.Model):

    name = models.CharField(max_length=32, blank=False, unique=True)
    def_value = models.FloatField()
    value = models.PositiveIntegerField()
    is_fortified = models.BooleanField(default=False)
    in_region = models.ForeignKey(Region, on_delete=models.CASCADE)
    owner = models.ForeignKey(Nation, related_name='owner', null=True, default=None, on_delete=models.CASCADE)
    troops = models.PositiveIntegerField()
    ships = models.PositiveIntegerField()
    is_land_locked = models.BooleanField(default=False)
    factory = models.ForeignKey(Nation, blank=True, null=True, related_name='factory',
                                default=None, on_delete=models.CASCADE)
    x_pos = models.IntegerField(null=False)
    y_pos = models.IntegerField(null=False)

    def __str__(self):
        return self.name


class AdjacentTerritory(models.Model):
    name = models.ForeignKey(Territory, on_delete=models.CASCADE)
    adj_label = name.name
    adjacent = models.ManyToManyField(Territory, blank=True, related_name='adjacent')


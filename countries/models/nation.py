from django.db import models
from django.contrib.auth.models import User


class Nation(models.Model):

    usr = models.ForeignKey(User, on_delete=models.CASCADE, null=True,
                            default=None)
    name = models.CharField(max_length=35, blank=False)
    bank = models.IntegerField(default=0)
    army_cost = models.IntegerField(null=False)
    army_attack = models.IntegerField(null=False)
    army_defense = models.IntegerField(null=False)
    navy_cost = models.IntegerField(null=False)
    navy_attack = models.IntegerField(null=False)
    navy_defense = models.IntegerField(null=False)


from django.db import models


from countries.models import Nation, Territory


class Army(models.Model):

    owner = models.ForeignKey(Nation, related_name='army_owner', default=None, on_delete=models.CASCADE)
    troops = models.IntegerField(null=False)
    region_from = models.ForeignKey(Territory, related_name='army_from', on_delete=models.CASCADE, default=None)
    region_to = models.ForeignKey(Territory, related_name='army_to', on_delete=models.CASCADE)
    move_distance = models.IntegerField(null=True)


class Navy(models.Model):

    owner = models.ForeignKey(Nation, related_name='navy_owner', default=None, on_delete=models.CASCADE)
    ships = models.IntegerField()
    region_from = models.ForeignKey(Territory, related_name='navy_from', on_delete=models.CASCADE)
    region_to = models.ForeignKey(Territory, related_name='navy_to', on_delete=models.CASCADE)
    troops = models.ForeignKey(Army, default=None, on_delete=models.CASCADE, null=True)
    move_distance = models.IntegerField(null=False)


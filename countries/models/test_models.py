from django.contrib.auth.models import User
from django.test import TestCase

from countries.models import Nation, Region, Territory


class TestModels(TestCase):

    def setUp(self):
        self.user = User.objects.create(username='me', password='12345')

    def test_nation(self):
        my_country = Nation(
            name='USA',
            bank=10000,
            army_cost=100,
            army_attack=4,
            army_defense=4,
            navy_cost=100,
            navy_attack=4,
            navy_defense=4,
            increment=0.5
        )
        self.assertIsInstance(my_country, Nation)

    def test_region_and_territory(self):

        my_region = Region(name='North America')
        my_territory = Territory(
            name='Western US',
            def_value=4,
            value=1000,
            in_region=my_region,
            troops=1100,
            navy=5550
        )
        self.assertIsInstance(my_region, Region)
        self.assertIsInstance(my_territory, Territory)

